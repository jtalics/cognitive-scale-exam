package com.foobar;

import java.util.ArrayList;
import java.util.List;

import static com.foobar.Ship.*;

/**
 * A Step is one line in the Script which contains 0 to 2 actions.
 * (NOTE: The requirements were vague on the ordering and uniqueness of the Actions.)
 */
class Step extends ArrayList<Action> {

    Step(List<Action> actions) {
        if (size() > 2) {
            throw new RuntimeException("too many Action on this Step - see requirements");
        }
        addAll(actions);
    }
}
