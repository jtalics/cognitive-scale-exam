package com.foobar;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

import static com.foobar.Ship.Action.Vector;
import static com.foobar.Field.*;

/**
 * A Field is a minefield that records the locations of all known mines in a cuboid.
 * It is specified in a file provided by the instructor.
 */
class Field extends ArrayList<Coordinate> implements ShipListener {

    static class Coordinate {
        final int x, y, z;

        Coordinate(int x, int y, int z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Coordinate that = (Coordinate) o;

            if (x != that.x) return false;
            if (y != that.y) return false;
            return z == that.z;

        }

        @Override
        public int hashCode() {
            int result = x;
            result = 31 * result + y;
            result = 31 * result + z;
            return result;
        }
    }

    private final File file;
    private final List<Integer[]> cuboid = new ArrayList<>(); // stored in km, displayed with letters
    private final List<Integer[]> origCuboid = new ArrayList<>();
    private int initialMineCount;

    Field(File file) throws FileNotFoundException {
        this.file = file;
        read();
        calcCoords();
        origCuboid.addAll(cuboid);
        //System.out.println("INITIAL\n"+this+"ENDINITIAL");
    }

    /**
     * Read the cuboid minefield from the specified file
     *
     * @throws FileNotFoundException
     */
    private void read() throws FileNotFoundException {
        Scanner scanner = new Scanner(file);
        String line;
        int lineLength = -1;
        while (scanner.hasNextLine()) {
            line = scanner.nextLine();
            if (lineLength == -1) lineLength = line.length();
            if (lineLength != line.length()) throw new RuntimeException("bad line length");
            Integer[] kilometers = new Integer[lineLength];
            cuboid.add(kilometers);
            char[] cs = line.toCharArray();
            for (int i = 0; i < cs.length; i++) {
                if (!isCuboidFormat(cs[i])) throw new RuntimeException("bad char = " + cs[i]);
                kilometers[i] = toKm(cs[i]);
            }
        }
    }

    private boolean isCuboidFormat(char c) {
        return (c == '.' || Character.isAlphabetic(c));
    }

    private Integer toKm(char c) {
        if (c == '.') return null; // signifies no mine present
        if (Character.isLowerCase(c)) return c -'a';
        return c - 'A' + 26;
    }

    private char fromKm(Integer km) {
        if (km == null) return '.'; // indicates no mine present
        if (km < 0) return '*'; // passed mine
        if (km <= 'z'-'a') return (char)(km + 'a');
        return (char)(km -26 + 'A');
    }

    /**
     * Normally we would use a queue here to handle event asynchronously with a separate thread, but
     * the Ship is blocked until the field is updated synchronously, so we can't go async.
     */
    @Override
    public void eventOccurred(Event event) {
        process(event);
    }

    private void process(Event event) {

        if (event.action instanceof Ship.Fire) {
            Ship.Fire fire = (Ship.Fire) event.action;
            sendVolley(event.location, fire.vectors);
        } else if (event.action instanceof Ship.Move) {
            // Ignore handling event for now, but leave stub here for next version
        } else if (event.action instanceof Ship.StepStart) {
            calcCuboid(event.location);
            System.out.print(this);
        } else if (event.action instanceof Ship.StepDone) {
            if (size() == 0) {
                emit(new FieldListener.Event(FieldListener.Event.Action.Complete, event.source));
            } else if (get(0) != null && get(0).z < event.location.z) {
                emit(new FieldListener.Event(FieldListener.Event.Action.Missed, event.source));
            }
            calcCuboid(event.location);
            System.out.print(this);
        }
    }

    /*
     * Show a cuboid which center is the point-of-view of the ship and all mines are displayed
     * The cuboid is a view/presentation and the coordinates are the model/data we work with.
     */
    private void calcCuboid(Coordinate pov) {
        int X = 0; // distance of farthest mine from POV
        int Y = 0;

        // Determine height and width of grid
        for (Coordinate m : this) { // get each mine coord
            if (X < Math.abs(m.x - pov.x)) X = Math.abs(m.x - pov.x);
            if (Y < Math.abs(m.y - pov.y)) Y = Math.abs(m.y - pov.y);
        }
        int height = 2 * Y + 1;
        int width = 2 * X + 1;
        // X,Y are now center of cuboid
        cuboid.clear();
        for (int row = 0; row < height; row++) cuboid.add(new Integer[width]);
        // cuboid.get(maxY)[maxX] = -22; // crosshair
        for (Coordinate m : this) cuboid.get(- m.y + pov.y + Y)[m.x - pov.x + X] = m.z - pov.z;
    }

    /*
     * Apply the volley (alpha, beta, gamma, delta) and remove mines that are hit.
     * Note that a hit does NOT depend on the z-coord.
     * Returns whether all mines were removed at the from-depth
     */
    private void sendVolley(Coordinate fromLocation, List<Vector> directions) {

        for (Vector dir : directions) {
            int x = fromLocation.x + dir.x;
            int y = fromLocation.y + dir.y;
            Coordinate mineLoc;
            Iterator<Coordinate> iter = iterator();
            while (iter.hasNext()) {
                mineLoc = iter.next();
                if (x == mineLoc.x && y == mineLoc.y) {
                    iter.remove(); // boom!  mine removed.  nice.
                    break;
                }
            }
        }
    }

    /**
     * Convert the cuboid to minefield coordinates during initialization.
     */
    private void calcCoords() {

        clear();
        // Select the Field's coordinate system by choosing the origin at center of z=0;
        int height = cuboid.size();
        int width = 0;
        if (height > 0) {
            width = cuboid.get(0).length;
        }
        // Note how we arbitrarily chose center for cuboids with _even_ dimensions
        int originY = height / 2;
        int originX = width / 2;

        // Place each mine in the Field's new coordinate system
        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                Integer z = cuboid.get(row)[col];
                if (z != null) {
                    add(new Coordinate(col - originX, row - originY, z));
                    // We keep this list sorted for efficiency
                    // because we process only the next z-level
                    // for each drop in the cuboid
                    Collections.sort(this, (o1, o2) -> o1.z - o2.z);
                }
            }
        }
        initialMineCount = size(); // needed for final scoring
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Integer[] row : cuboid) {
            for (Integer i : row) {
                if (i == null) {
                    sb.append('.');
                } else {
                    sb.append(fromKm(i));
                }
            }
            sb.append('\n');
        }
        return sb.toString();
    }

    public int getInitialMineCount() {
        return initialMineCount;
    }

    public void reset() {
        cuboid.clear();
        cuboid.addAll(origCuboid);
    }

    private final List<FieldListener> listeners = new ArrayList<>();

    void addListener(FieldListener listener) {
        listeners.add(listener);
    }

    private void emit(FieldListener.Event event) {
        for (FieldListener l : listeners) {
            l.eventOccurred(event);
        }
    }
}
