package com.foobar;


interface FieldListener {
    void eventOccurred(Event event);

    /*
         * Observer pattern machinery follows.
         * We use this pattern because the Field may detect 'Game Over' situations
         * that it needs to tell the Evaluator
         */
    class Event {

        enum Action {
            Missed,
            Complete
        }

        final Action action; // verb
        final Ship ship;

        Event(Action action, Ship ship) {
            this.action = action;
            this.ship = ship;
        }
    }
}
