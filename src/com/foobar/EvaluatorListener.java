package com.foobar;

public interface EvaluatorListener {

    void eventOccurred(Event event);
    class Event {

        enum Action {
            Start,
            Stop
        }

        final Action action;
        final Ship ship;

        Event(Action action, Ship ship) {
            this.action = action;
            this.ship = ship;
        }
    }
}
