package com.foobar;

import com.foobar.Field.Coordinate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A Ship clears mines from the mine field by doing two types of actions: Fire and Move.
 * It broadcasts these actions to its listeners (e.g. the mine field) which can update if desired.
 */
public class Ship implements EvaluatorListener {

    interface Action {
        class Vector {
            final int x, y;

            Vector(int x, int y) {
                this.x = x;
                this.y = y;
            }
        }
    }

    class StepStart implements Action {

    }

    class StepDone implements Action {

    }

    enum Fire implements Action {
        alpha(new Vector(-1, -1), new Vector(-1, 1), new Vector(1, -1), new Vector(1, 1)),
        beta(new Vector(-1, 0), new Vector(0, -1), new Vector(0, 1), new Vector(1, 0)),
        gamma(new Vector(-1, 0), new Vector(0, 0), new Vector(1, 0)),
        delta(new Vector(0, -1), new Vector(0, 0), new Vector(0, 1));

        Fire(Vector... vs) {
            Collections.addAll(vectors, vs);
        }

        final List<Vector> vectors = new ArrayList<>();
    }

    enum Move implements Action {
        north(new Vector(0, 1)),
        south(new Vector(0, -1)),
        east(new Vector(1, 0)),
        west(new Vector(-1, 0));
        // Warning: if you use anything other that 0 or 1's above,
        // then you need to consider modifying Evaluation.movePenalty
        final Vector vector;

        Move(Vector vector) {
            this.vector = vector;
        }
    }

    final Script script;
    private Coordinate location;
    private boolean active; // still unpassed mines to clear
    int stepsExecuted;

    Ship(Script script) {
        this.script = script;
        this.location = new Coordinate(0, 0, 0); // Ship always starts at Field's origin
    }

    private void act(Action action) {
        if (action instanceof Move) {
            int x = ((Move) action).vector.x;
            int y = ((Move) action).vector.y;
            translate(x, y, 0);
        }
        emit(new ShipListener.Event(this, action, location));
    }

    private void executeScript() {
        active = true;
        for (stepsExecuted = 0; stepsExecuted < script.size() && active; stepsExecuted++) {
            System.out.println("Step " + (stepsExecuted + 1));
            System.out.println();
            act(new StepStart());
            Step step = script.get(stepsExecuted);
            System.out.println();
            for (Action action : step) {
                System.out.print(action + " ");
                act(action);
            }
            System.out.println();
            System.out.println();
            translate(0, 0, 1); // drop down since step is complete
            act(new StepDone());
            System.out.println();
        }
    }

    private void translate(int x, int y, int z) {
        location = new Coordinate(location.x + x, location.y + y, location.z + z);
    }

    private final List<ShipListener> listeners = new ArrayList<>();

    void addListener(ShipListener listener) {
        listeners.add(listener);
    }

    private void emit(ShipListener.Event event) {
        for (ShipListener l : listeners) {
            l.eventOccurred(event);
        }
    }

    @Override
    public void eventOccurred(EvaluatorListener.Event event) {
        switch (event.action) {
            case Start:
                executeScript();
                break;
            case Stop:
                active = false; // this works ONLY because boolean check occurs at end of loop
                break;
        }
    }
}
