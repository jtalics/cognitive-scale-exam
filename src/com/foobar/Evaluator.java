package com.foobar;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.foobar.Evaluator.Report.Status.*;
import static com.foobar.EvaluatorListener.Event.Action.*;

/**
 * An Evaluator executes the provided Script in the mine Field and scores the scripts.
 * You might think about the Evaluator as an umpire on a football field with Ships as players.
 * A report is written to System.out.
 * Usage: java Evaluator [field-file][ship-script-files...]
 */
public class Evaluator implements FieldListener, Runnable {

    private final String fieldFileName;
    private final String[] scriptFileNames;

    private Evaluator(String fieldFileName, String... scriptFileNames) {
        this.fieldFileName = fieldFileName;
        this.scriptFileNames = scriptFileNames;
    }

    @Override
    public void run() {
        try {
            Field field = new Field(new File(fieldFileName));
            field.addListener(this);
            for (String s :scriptFileNames) {
                Script script = new Script(new File(s));
                Ship ship = new Ship(script);
                addListener(ship);
                ship.addListener(field);
                emit(new EvaluatorListener.Event(Start, ship));
                Report report = score(field, ship);
                System.out.println(report);
                field.reset();
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    static class Report {
        enum Status {
            pass,
            fail
        }

        final Status status;
        final int grade;

        Report(Status status, int grade) {
            this.status = status;
            this.grade = grade;
        }

        @Override
        public String toString() {
            return status+" ("+ grade + ")";
        }
    }

    private static Report score(Field field, Ship ship) {
        if (field.size() > 0) return new Report(fail,0);
        if (ship.stepsExecuted < ship.script.size()) return new Report(pass,1);

        int mineCountInitial = field.getInitialMineCount();
        int fireCount = ship.script.getFireCount();
        int moveCount = ship.script.getMoveCount();

        int firePenalty = 5 * fireCount;
        if (firePenalty > 5 * mineCountInitial) firePenalty = 5 * mineCountInitial;
        int movePenalty = 2 * moveCount;
        if (movePenalty > 3 * mineCountInitial) movePenalty = 3 * mineCountInitial;

        return new Report(pass,10 * mineCountInitial - firePenalty - movePenalty);
    }

    @Override
    public void eventOccurred(FieldListener.Event event) {
        switch(event.action) {

            case Missed:
                emit(new EvaluatorListener.Event(Stop, event.ship));
                break;
            case Complete:
                emit(new EvaluatorListener.Event(Stop, event.ship));
                break;
        }
    }

    private final List<EvaluatorListener> listeners = new ArrayList<>();

    private void addListener(EvaluatorListener listener) {
        listeners.add(listener);
    }

    private void emit(EvaluatorListener.Event event) {
        for (EvaluatorListener l:listeners) {
            l.eventOccurred(event);
        }
    }

    public static void main(String[] args) throws Exception {

        if (args.length<2) {
            throw new IllegalArgumentException("USAGE: java Evaluator <field> <scripts...>");
        }
        String[] ss = new String[args.length-1];
        System.arraycopy(args, 1, ss, 0, args.length - 1);
        new Evaluator(args[0],ss).run();
    }
}
