package com.foobar;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import com.foobar.Ship.*;

import java.util.List;
import java.util.Scanner;

/**
 * A Script is an ordered list of steps that are applied to the mine field by the Ship.
 * It is specified in a file that the cadet writes to be grades.
 */
class Script extends ArrayList<Step> {
    private final File file;
    private int fireCount=0;
    private int moveCount=0;

    Script(File file) throws FileNotFoundException {
        this.file = file;
        read();
    }

    private void read() throws FileNotFoundException {
        Scanner scanner = new Scanner(file);
        int lineCount=0;
        String line;
        while (scanner.hasNextLine()) {
            List<Action> actions = new ArrayList<>();
            line=scanner.nextLine().trim();
            if(++lineCount > 'z'-'A') throw new RuntimeException("script file "+file.getName()+" has too many lines ");
            if (!line.isEmpty()) {
                String[] actionNames = line.split("\\s+");
                for (String name : actionNames) {
                    Action action = matchActionName(name);
                    actions.add(action);
                    if (action != null) {
                        if (action instanceof Fire) fireCount++;
                        else if (action instanceof Move) moveCount++;
                    }
                }
            }
            add(new Step(actions));
        }
    }

    private Action matchActionName(String name) {
        Action retval=null;
        try {retval = Fire.valueOf(name);}
        catch(IllegalArgumentException e) {} // ignore
        if (retval != null) return retval;
        try {retval = Move.valueOf(name);}
        catch(IllegalArgumentException e) {} // ignore
        if (retval == null) throw new RuntimeException("unknown action: "+name);
        return retval;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Step step : this) {
            sb.append(step.toString());
        }
        return sb.toString();
    }

    public int getFireCount() {
        return fireCount;
    }

    public int getMoveCount() {
        return moveCount;
    }

}
