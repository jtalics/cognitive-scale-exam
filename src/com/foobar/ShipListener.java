package com.foobar;

interface ShipListener {
    void eventOccurred(Event event);

    // Observer pattern machinery follows.
    // We use this pattern because there may be multiple Ships clearing the same minefield next version.
    class Event {
        final Ship source;
        final Ship.Action action;
        final Field.Coordinate location;

        Event(Ship source, Ship.Action action, Field.Coordinate location) {
            this.source = source;
            this.action = action;
            this.location = location;
        }
    }
}
